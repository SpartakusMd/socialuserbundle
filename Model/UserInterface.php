<?php

namespace SpartakusMd\SocialUserBundle\Model;

interface UserInterface
{
    /**
     * Get id.
     *
     * @return int
     */
    public function getId();

    /**
     * Set usernameSlug.
     *
     * @param string $usernameSlug
     *
     * @return \SpartakusMd\SocialUserBundle\Model\UserInterface
     */
    public function setUsernameSlug($usernameSlug);

    /**
     * Get usernameSlug.
     *
     * @return string
     */
    public function getUsernameSlug();


    /**
     * Set email.
     *
     * @param string $email
     *
     * @return \SpartakusMd\SocialUserBundle\Model\UserInterface
     */
    public function setEmail($email);

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail();

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return \SpartakusMd\SocialUserBundle\Model\UserInterface
     */
    public function setUpdatedAt($updatedAt);

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt();

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return \SpartakusMd\SocialUserBundle\Model\UserInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * Add identities.
     *
     * @param \SpartakusMd\SocialUserBundle\Model\UserIdentity $identity
     *
     * @return \SpartakusMd\SocialUserBundle\Model\UserInterface
     */
    public function addIdentity(UserIdentity $identity);

    /**
     * Remove identities.
     *
     * @param \SpartakusMd\SocialUserBundle\Model\UserIdentity $identity
     *
     * @return \SpartakusMd\SocialUserBundle\Model\UserInterface
     */
    public function removeIdentity(UserIdentity $identity);

    /**
     * Get identities.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdentities();

    /**
     * Get identities.
     *
     * @return array
     */
    public function getIdentitiesAsStrings();

    /**
     * Add emailChangeRequest.
     *
     * @param \SpartakusMd\SocialUserBundle\Model\UserEmailChangeRequest $emailChangeRequest
     *
     * @return \SpartakusMd\SocialUserBundle\Model\UserInterface
     */
    public function addEmailChangeRequest(UserEmailChangeRequest $emailChangeRequest);

    /**
     * Remove emailChangeRequest.
     *
     * @param \SpartakusMd\SocialUserBundle\Model\UserEmailChangeRequest $emailChangeRequest
     *
     * @return \SpartakusMd\SocialUserBundle\Model\UserInterface
     */
    public function removeEmailChangeRequest(UserEmailChangeRequest $emailChangeRequest);

    /**
     * Get emailChangeRequest.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmailChangeRequests();
}
